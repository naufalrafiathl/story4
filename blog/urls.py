from django.urls import path
from . import views
from django.contrib import admin

app_name = 'blog'

urlpatterns = [
    path('', views.blog, name='index'),
    path('experience', views.exp, name='exp'),
    path('education', views.edu, name='edu'),
    path('contact', views.cont, name='cont'),
    
]