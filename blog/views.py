from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.

def blog(request):
    return render(request, 'index.html')

def exp(request):
    return render(request, 'exp.html')

def edu(request):
    return render(request, "edu.html")
def cont(request):
    return render(request, "contact.html")